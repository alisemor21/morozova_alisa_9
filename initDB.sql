CREATE DATABASE university;

USE university;

CREATE TABLE person(
    id INTEGER primary key, 
    firstName TEXT,
    secondName TEXT,
    dateOfBirth TEXT
);

CREATE TABLE recordBook(
    personID INTEGER primary key, 

    1_lesson TEXT, 
    1_date TEXT, 
    1_teacher TEXT, 
    1_mark INTEGER,

    2_lesson TEXT, 
    2_date TEXT, 
    2_teacher TEXT, 
    2_mark INTEGER,

    3_lesson TEXT, 
    3_date TEXT, 
    3_teacher TEXT, 
    3_mark INTEGER,

    4_lesson TEXT, 
    4_date TEXT, 
    4_teacher TEXT, 
    4_mark INTEGER,

    5_lesson TEXT, 
    5_date TEXT, 
    5_teacher TEXT,
    5_mark INTEGER);

INSERT INTO person VALUES(1, "Adamson", " April", "20/12/1998");
INSERT INTO person VALUES(2, "Evans", "Amber", "11/05/1999");
INSERT INTO person VALUES(3, "Flatcher", "Daisy", "03/08/1999");
INSERT INTO person VALUES(4, "Gilbert", "Fred", "31/12/1977");

INSERT INTO recordBook VALUES(1, 
"Chemistry", "26/12/2022", "King A.A.", 5,
"Geometry", "26/12/2022", "Moore V.N.", 5,
"Literature", "27/12/2022", "Davis M.J.", 5,
"History", "28/12/2022", "Brown P.P.", 5,
"Biology", "29/12/2022", "King A.P.", 5);

INSERT INTO recordBook VALUES(2, 
"Chemistry", "26/12/2022", "King A.A.", 5,
"Literature", "27/12/2022", "Davis M.J.", 4,
"Biology", "29/12/2022", "King A.P.", 5,
null, null, null, null,
null, null, null, null);

INSERT INTO recordBook VALUES(3, 
"Chemistry", "26/12/2022", "King A.A.", 4,
"Geometry", "26/12/2022", "Moore V.N.", 3,
"History", "28/12/2022", "Brown P.P.", 3,
"Biology", "29/12/2022", "King A.P.", 2,
null, null, null, null);

INSERT INTO recordBook VALUES(4, 
"Geometry", "26/12/2022", "Moore V.N.", 3,
"History", "28/12/2022", "Brown P.P.", 2,
"Biology", "29/12/2022", "King A.P.", 2,
null, null, null, null,
null, null, null, null);

COMMIT;

